﻿using BloodDonorApp.Application.Interfaces;
using BloodDonorApp.Application.ViewModel;
using BloodDonorApp.DataLayer.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonorApp.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IBloodDonorService _bloodDonorService;

        public AccountController(IBloodDonorService bloodDonorService)
        {
            _bloodDonorService = bloodDonorService;
        }
        [HttpGet]
        public IActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> SignUp(CreateSignUpUserModel signUpUser)
        {
            var result = await _bloodDonorService.CreateAsync(signUpUser);
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }
            ModelState.Clear();
            return View();
        }
        [HttpGet]
        public IActionResult SignIn()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> SignIn(CreateSignInUserModel signInUser)
        {
            var result = await _bloodDonorService.PasswordSignInAsync(signInUser);
           if(result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        public async Task<IActionResult> SignOut()
        {
            await _bloodDonorService.SignOutAsync();
            return RedirectToAction("Index","Home");
        }

        [HttpGet]
        public async Task<IActionResult> GetDonorList()
        {
            var donors = await _bloodDonorService.GetDonorListAsync();
            return View(donors);
        }
        [HttpGet]
        public IActionResult ChangePassWord()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ChangePassWord(ChangePasswordViewModel newpass)
        {
            var result =await _bloodDonorService.ChangePasswordAsync(newpass.CurrentPassword, newpass.NewPassword);
            await _bloodDonorService.SignOutAsync();
            return RedirectToAction("SignIn");
        }
    }
}
