﻿using BloodDonorApp.Application.Interfaces;
using BloodDonorApp.Application.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BloodDonorApp.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AuthorizationController : Controller
    {
        private readonly IBloodDonorService _bloodDonorService;

        public AuthorizationController(IBloodDonorService bloodDonorService)
        {
            _bloodDonorService = bloodDonorService;
        }

        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateRole(CreateRoleViewModel role)
        {
            var result =await _bloodDonorService.CreateRoleAsync(role);
            if(!result.Succeeded)
            {
                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError("",error.Description);
                }
                ModelState.Clear();
            }
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> RoleList()
        {
            var result = await _bloodDonorService.GetRoleListAsync();
            return View(result);
        }
        [HttpGet]
        public async Task<IActionResult> EditUserInRole(string id)
        {
            var rs =await _bloodDonorService.EditUserInRole(id);
            return View(rs);
        }
        [HttpPost]
        public async Task<IActionResult> EditUserInRole(string id, List<EditUserInRoleViewModel> model)
        {
            await _bloodDonorService.AssignUserInRoleAsync(id, model);

            return RedirectToAction("RoleList");
        }
    }
}
