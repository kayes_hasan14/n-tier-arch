﻿using BloodDonorApp.DataLayer.Interfaces;
using Microsoft.AspNetCore.Http;
using BloodDonorApp.Entites;
using BloodDonorApp.Entites.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonorApp.DataLayer.Repositories
{
   
    public class BloodDonorRepository : IBloodDonorRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IHttpContextAccessor _httpContext;

        public BloodDonorRepository(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, 
            RoleManager<IdentityRole> roleManager, IHttpContextAccessor httpContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _httpContext = httpContext;
        }



        public async Task<IdentityResult> CreateAsync(SignUpUserModel signUpUser)
        {
            if (!_userManager.Users.Any())
            {

                var user = new ApplicationUser();
                user.Email = signUpUser.Email;
                user.UserName = signUpUser.Email;
                user.Name = signUpUser.Name;
                user.LastDonated = signUpUser.LastDonated;
                user.BloodGroup = signUpUser.BloodGroup;
                user.DateOfBirth = signUpUser.DateOfBirth;


                var rs = await _userManager.CreateAsync(user, signUpUser.Password);
                var newRole = new IdentityRole()
                {
                    Name = "Admin"
                };
                var role = await _roleManager.CreateAsync(newRole);
                var result = await _userManager.AddToRoleAsync(user, newRole.Name);
                return rs;
            }
            else
            {

                var user = new ApplicationUser();
                user.Email = signUpUser.Email;
                user.UserName = signUpUser.Email;
                user.Name = signUpUser.Name;
                user.LastDonated = signUpUser.LastDonated;
                user.BloodGroup = signUpUser.BloodGroup;
                user.DateOfBirth = signUpUser.DateOfBirth;


                return await _userManager.CreateAsync(user, signUpUser.Password);
            }

        }

        public async Task<List<ApplicationUser>> GetDonorListAsync()
        {
            return await _userManager.Users.OrderByDescending(x => x.LastDonated).ToListAsync();
        }

        public async Task<SignInResult> PasswordSignInAsync(SignInUserModel signInUser)
        {
            return await _signInManager.PasswordSignInAsync(signInUser.Email, signInUser.Password, signInUser.rememberMe, false);
        }

        public async Task SignOutAsync()
        {
            await _signInManager.SignOutAsync();
        }



        public async Task<IdentityResult> CreateRoleAsync(RoleModel roleModel)
        {
            var role = new IdentityRole();
            role.Name = roleModel.RoleName;
            return await _roleManager.CreateAsync(role);
        }

        public async Task<List<IdentityRole>> GetRoleListAsync()
        {
            return await _roleManager.Roles.OrderBy(x => x.Name).ToListAsync();
        }

        public async Task<IdentityRole> FindByIdAsync(string id)
        {
            return await _roleManager.FindByIdAsync(id);
        }
        public async Task<bool> IsInRoleAsync(ApplicationUser user, string roleName)
        {

            return await _userManager.IsInRoleAsync(user, "Admin");
        }

        public async Task<bool> AddUsersInRoleAsync(string userId, string roleId)
        {
            var role = await _roleManager.FindByIdAsync(roleId);
            var user = await _userManager.FindByIdAsync(userId);
            if (!await _userManager.IsInRoleAsync(user, role.Name))
            {
                var result = await _userManager.AddToRoleAsync(user, role.Name);
                if (result.Succeeded)
                    return true;
            }
            return false;
        }

        public async Task<bool> RemoveUsersFromRoleAsync(string userId, string roleId)
        {
            var role = await _roleManager.FindByIdAsync(roleId);
            var user = await _userManager.FindByIdAsync(userId);
            if (await _userManager.IsInRoleAsync(user, role.Name))
            {
                var result = await _userManager.RemoveFromRoleAsync(user, role.Name);
                if (result.Succeeded)
                    return true;
            }
            return false;
        }

        public async Task<bool> IsInRoleAsync(string userId, string roleName)
        {
            var user =await _userManager.FindByIdAsync(userId);
            if(await _userManager.IsInRoleAsync(user, roleName))
            {
                return true;
            }
            return false;
        }

        public Task GetUserIdAsync(ApplicationUser user)
        {
            throw new System.NotImplementedException();
        }

        public async Task<bool> ChangePasswordAsync(string password, string newPass)
        {
            var user =await _userManager.GetUserAsync(_httpContext.HttpContext.User);
            var result =await _userManager.ChangePasswordAsync(user, password, newPass);
            if(result.Succeeded)
            {
                return true;
            }
            return false;
        }



        

    }


}

