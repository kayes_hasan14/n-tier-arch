﻿using BloodDonorApp.Entites;
using BloodDonorApp.Entites.Model;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BloodDonorApp.DataLayer.Interfaces
{
    public interface IBloodDonorRepository
    {
        Task<IdentityResult> CreateAsync(SignUpUserModel signUpUser);
        Task<SignInResult> PasswordSignInAsync(SignInUserModel signInUser);
        Task SignOutAsync();
        Task<List<ApplicationUser>> GetDonorListAsync();
        Task<IdentityResult> CreateRoleAsync(RoleModel roleModel);
        Task<List<IdentityRole>> GetRoleListAsync();
        Task<IdentityRole> FindByIdAsync(string id);
        Task<bool> IsInRoleAsync(string userId, string roleName);
        Task<bool>AddUsersInRoleAsync(string userId, string roleId);
        Task<bool> RemoveUsersFromRoleAsync(string userId, string roleId);
        Task GetUserIdAsync(ApplicationUser user);
        Task<bool> ChangePasswordAsync(string password, string newPass); 
        
    }
}
