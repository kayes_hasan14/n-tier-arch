﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BloodDonorApp.Entites
{
    public class SignUpUserModel
    {
        public string Id { get; set; }

        public string Name { get; set; }
        
        public string Email { get; set; }

       
        public string Password { get; set; }
        
        public string ConfirmPassword { get; set; }

        
        public DateTime DateOfBirth { get; set; }
       
        public string BloodGroup { get; set; }
        
        public DateTime LastDonated { get; set; }

    }
}
