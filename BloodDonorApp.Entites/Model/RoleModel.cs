﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodDonorApp.Entites.Model
{
    public class RoleModel
    {
        public string Id { get; set; }
        public string RoleName { get; set; }
    }
}
