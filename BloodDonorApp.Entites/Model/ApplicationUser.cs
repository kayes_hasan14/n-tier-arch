﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace BloodDonorApp.Entites.Model
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string BloodGroup { get; set; }
        public DateTime LastDonated { get; set; }
    }
}
