﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BloodDonorApp.Entites.Model
{
    public class SignInUserModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool rememberMe { get; set; }
    }
}
